(function () {
  'use strict';

  /* @ngdoc object
   * @name demo
   * @description
   *
   */
  angular
    .module('demo', [
      'ngMaterial',
      'ui.router',
      'home',
      'oc.lazyLoad'
    ]);
}());
